﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace NpcMasterListUpdater
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        private string masterList = "";
        public string MasterList
        {
            get
            {
                return masterList;
            }
            set
            {
                masterList = value;
                OnPropertyChanged("MasterList");
            }
        }

        private string changes = "";
        public string Changes
        {
            get
            {
                return changes;
            }
            set
            {
                changes = value;
                OnPropertyChanged("Changes");
            }
        }

        private string table = "";
        public string Table
        {
            get
            {
                return table;
            }
            set
            {
                table = value;
                OnPropertyChanged("Table");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        private ICommand goCommand;
        public ICommand GoCommand
        {
            get { return goCommand ?? (goCommand = new RelayCommand(param => Go())); }
        }

        public void Go()
        {
            //execute on hitting go button
            List<string> masterListLines = MasterList.Split('\n').ToList();
            Dictionary<int, Npc> allNpcs = new Dictionary<int, Npc>();
            string title = "";

            foreach (string line in masterListLines)
            {
                Match mm = Regex.Match(line, "A list of all NPCs in .+\\.");

                if (mm.Success)
                {
                    title = line.TrimEnd('\r', '\n');
                    continue;
                }

                //huge crazy reg ex
                Match m = Regex.Match(line, "^\\[tr\\]\\[td\\](?<name>.+)\\s+\\((?<id>[0-9]+)\\)\\s*\\[/td\\]\\s+\\[td\\](?<bugs>.+?)\\[/td\\]\\s+\\[td\\]\\s+\\[.+?(?<position>✘|✔).+?\\]\\s+\\[/td\\]\\s+\\[td\\]\\s+\\[.+?(?<stats>✘|✔).+?\\]\\s+\\[/td\\]\\s+\\[td\\]\\s+\\[.+?(?<spells>✘|✔).+?\\]\\s+\\[/td\\]\\s+\\[td\\]\\s+\\[.+?(?<tested>✘|✔).+?\\]\\s+\\[/td\\]\\s+\\[/tr\\]");
                if(!m.Success)
                    continue;
               
                string name = m.Groups[1].ToString();
                int id = int.Parse(m.Groups[2].ToString());
                string bugs = m.Groups[3].ToString();
                bool position = tickOrCross(m.Groups[4].ToString());
                bool stats = tickOrCross(m.Groups[5].ToString());
                bool spells = tickOrCross(m.Groups[6].ToString());
                bool tested = tickOrCross(m.Groups[7].ToString());


                allNpcs.Add(id, new Npc{id = id, name = name, bugs = bugs, position = position, stats = stats, spells = spells, tested = tested});
            }

            List<string> changesLines = Changes.Split('\n').ToList();
            Dictionary<int, string> fixedNpcs = new Dictionary<int, string>();

            foreach (string changesLine in changesLines)
            {

                Match m = Regex.Match(changesLine, "^([0-9]+) .+ - (\\w+)");
                if (!m.Success)
                    continue;

                int id = int.Parse(m.Groups[1].ToString());
                string msg = m.Groups[2].ToString();

                fixedNpcs.Add(id, msg);
            }

            foreach (KeyValuePair<int, string> kvp in fixedNpcs)
            {
                switch (kvp.Value)
                {
                    case "Positioned":
                        allNpcs[kvp.Key].position = true;
                        break;
                    case "Tested":
                        allNpcs[kvp.Key].tested = true;
                        break;
                    case "Spells":
                        allNpcs[kvp.Key].spells = true;
                        break;
                    case "Done":
                        allNpcs[kvp.Key].stats = true;
                        allNpcs[kvp.Key].spells = true;
                        allNpcs[kvp.Key].tested = true;
                        break;
                    case "Sort":
                        allNpcs[kvp.Key].stats = true;
                        allNpcs[kvp.Key].spells = true;
                        break;
                }

            }

            string npcTable;

            //headings and key
            npcTable = title;
            npcTable +=
                "\n\n\n\n Table Key:\n[color=#32CD32] ✔ [/color] = Done\n[color=#FF0000] ✘ [/color] = Not Done \n\nBugs are coloured by severity: [color=#32CD32]minor[/color], [color=#FFA500]moderate[/color], [color=#FF0000]critical[/color]\n\n\n\n";

            //table set up
            npcTable += "[table=test]\n";
            //table headings
            npcTable +=
                "[tr][th][color=#000000]NPC/ID[/color][/th]  [th] [color=#000000]Bugs (By Post ID)[/color][/th]  [th][color=#000000]Position[/color][/th]  [th][color=#000000]Stats[/color][/th] [th][color=#000000]Spells[/color][/th] [th][color=#000000]Spells Tested[/color][/th][/tr]\n";

            foreach (KeyValuePair<int, Npc> kvp in allNpcs)
            {
                npcTable += "[tr][td]" + kvp.Value.name + " (" + kvp.Key + ") [/td] [td] " + kvp.Value.bugs + " [/td] [td] " + tableCode(kvp.Value.position) + "[/td] [td] " + tableCode(kvp.Value.stats) + " [/td] [td] " + tableCode(kvp.Value.spells) + " [/td] [td] " + tableCode(kvp.Value.tested) + " [/td] [/tr]\n";

            }

            npcTable += "[/table]";
            Table = npcTable;
        }

        
        
        private bool tickOrCross(string status)
        {
            return status == "✔";
        }


        private string tableCode(bool status)
        {
            return status ? "[color=#32CD32] ✔ [/color]" : "[color=#FF0000] ✘ [/color]";
        }

        private ICommand clearCommand;
        public ICommand ClearCommand
        {
            get { return clearCommand ?? (clearCommand = new RelayCommand(param => Clear())); }
        }

        public void Clear()
        {
            MasterList = "";
            Changes = "";
            Table = "";
        }
    }

    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields

        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough]
        public bool CanExecute(object parameters)
        {
            return _canExecute == null ? true : _canExecute(parameters);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameters)
        {
            _execute(parameters);
        }

        #endregion // ICommand Members
    }
}
