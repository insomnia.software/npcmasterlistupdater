﻿namespace NpcMasterListUpdater
{
    public class Npc
    {
        public int id { get; set; }
        public string name { get; set; }
        public string bugs { get; set; }
        public bool position { get; set; }
        public bool stats { get; set; }
        public bool spells { get; set; }
        public bool tested { get; set; }
    }
}
