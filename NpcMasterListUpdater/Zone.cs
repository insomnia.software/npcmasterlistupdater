﻿using System.Collections.Generic;

namespace NpcMasterListUpdater
{
    public class Zone
    {
        protected bool Equals(Zone other)
        {
            return string.Equals(name, other.name);
        }

        public override int GetHashCode()
        {
            return (name != null ? name.GetHashCode() : 0);
        }

        public readonly string name;
        public List<Npc> npcs = new List<Npc>();

        public Zone(string name, List<Npc> npcs)
        {
            this.name = name;
            this.npcs = npcs;
        }

        public Zone(string name)
        {
            this.name = name;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Zone) obj);
        }
    }
}
